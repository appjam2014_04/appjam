package org.sopt.appjam;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class RoomAdd extends Activity {

	private static final int GET_PIC_CODE = 1;
	private Uri uri;
	final int REQ_CODE_SELECT_IMAGE = 100;
	protected static String res;

	private ImageButton img_btn;
	private Button btn_add;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_room_add);

		btn_add = (Button) findViewById(R.id.btn_moim_add);
		img_btn = (ImageButton) findViewById(R.id.btn_coverImage);

		btn_add.setOnClickListener(listener);
		img_btn.setOnClickListener(listener);
	}

	private OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent;
			switch (v.getId()) {
			case R.id.btn_coverImage:
				intent = new Intent(Intent.ACTION_PICK);
				intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
				intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				startActivityForResult(intent, REQ_CODE_SELECT_IMAGE);
				break;
			case R.id.btn_moim_add:
				String imgPath = getPath(uri);
				Log.d("DEBUG", "Choose: " + imgPath);
				try {
					new HttpUpload(RoomAdd.this, imgPath).execute().get();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Log.v("min",res);
				EditText Ename = (EditText) findViewById(R.id.edit_moim_name);
				EditText Epassword = (EditText) findViewById(R.id.edit_moim_password);
				String name = Ename.getText().toString();
				String password = Epassword.getText().toString();
				try {
					String JSONString = null;
					String URL = "http://54.178.153.85:3000/makeroom"; // server
																			// URL
					ArrayList<String> postInfo = new ArrayList<String>();
					postInfo.add(URL);
					postInfo.add(name);
					postInfo.add(password);
					postInfo.add(res);
					Post post = new Post();
					
					JSONString = post.execute(postInfo).get();
					
					Toast.makeText(getApplicationContext(), "생성", Toast.LENGTH_SHORT).show();
					finish();

				} catch (Exception e) {
					Log.v("min", "fail");
				}
				/* transfer to server */

				break;
			default:
				break;
			}
		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		/*
		if (requestCode == GET_PIC_CODE && resultCode == Activity.RESULT_OK) {
			Uri imgUri = data.getData();
			String imgPath = getPath(imgUri);
			Log.d("DEBUG", "Choose: " + imgPath);
			new HttpUpload(this, imgPath).execute();
		}*/

		Toast.makeText(getBaseContext(), "resultCode : " + resultCode,
				Toast.LENGTH_SHORT).show();

		if (requestCode == REQ_CODE_SELECT_IMAGE) {
			if (resultCode == Activity.RESULT_OK) {
				try {
					// Uri에서 이미지 이름을 얻어온다.
					//String name_Str = getImageNameToUri(data.getData());
					uri = data.getData();

					// 이미지 데이터를 비트맵으로 받아온다.
					Bitmap image_bitmap = Images.Media.getBitmap(
							getContentResolver(), data.getData());

					// 배치해놓은 ImageView에 set
					img_btn.setImageBitmap(image_bitmap);

					// Toast.makeText(getBaseContext(), "name_Str : "+name_Str ,
					// Toast.LENGTH_SHORT).show();

				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getImageNameToUri(Uri data) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(data, proj, null, null, null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

		cursor.moveToFirst();

		String imgPath = cursor.getString(column_index);
		String imgName = imgPath.substring(imgPath.lastIndexOf("/") + 1);

		return imgName;
	}

	// Get the path from Uri
	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		startManagingCursor(cursor);
		int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

}
