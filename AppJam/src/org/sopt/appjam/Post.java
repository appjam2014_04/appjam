package org.sopt.appjam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;

public class Post extends AsyncTask<ArrayList<String>, Void, String> {
	@Override
	protected String doInBackground(ArrayList<String>... passing) {
		String res = "";
		// Create a new HttpClient and Post Header
		ArrayList<String> passed = passing[0]; // get passed arraylist
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(passed.get(0));

		try {
			// Add your data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
					passed.size() - 1);
			for (int i = 1; i < passed.size(); i++)
				nameValuePairs.add(new BasicNameValuePair("param" + i, passed
						.get(i)));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "utf-8"));
			// Execute HTTP Post Request
			res = httpclient.execute(httppost, mResHandler);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		return res;
	}

	ResponseHandler<String> mResHandler = new ResponseHandler<String>() {
		public String handleResponse(HttpResponse response) {
			StringBuilder html = new StringBuilder();
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				for (;;) {
					String line = br.readLine();
					if (line == null)
						break;
					html.append(line + '\n');
				}
				br.close();

			} catch (Exception e) {
			}
			return html.toString();
		}
	};
}