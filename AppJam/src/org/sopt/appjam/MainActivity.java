package org.sopt.appjam;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity {

	private ListView listView = null;
	private ListViewAdapter mAdapter = null;
	private JSONArray ja;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		showActionBar();
		Intent intent = getIntent();

		listView = (ListView) findViewById(R.id.room_list);
		mAdapter = new ListViewAdapter(this);
		listView.setAdapter(mAdapter);
		listView.setOnItemClickListener(itemClickListenerOfLanguageList);

		String JSONString = intent.getStringExtra("roomList");

		try {
			ja = new JSONArray(JSONString);

			mAdapter.addItem(ja.getJSONObject(0).getString("name"),R.drawable.room1);

			mAdapter.addItem(ja.getJSONObject(1).getString("name"),R.drawable.room2);

			mAdapter.addItem(ja.getJSONObject(2).getString("name"),R.drawable.room3);

			mAdapter.addItem(ja.getJSONObject(3).getString("name"),R.drawable.room4);

			mAdapter.addItem("�游���",R.drawable.roomadd);

			for (int i = 0; i < ja.length(); i++) {

			}
		} catch (JSONException e) {
		}

		ImageButton btn_side = (ImageButton) findViewById(R.id.btn_side);
		btn_side.setOnClickListener(listener);

	}

	private OnItemClickListener itemClickListenerOfLanguageList = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> adapterView, View clickedView,
				int pos, long id) {
			Intent intent = new Intent(getApplicationContext(), RoomEx.class);
			if (ja.length() == pos) {
				/* add */
				intent = new Intent(getApplicationContext(),RoomAdd.class);
			} else {
				try {
					String JSONString = ja.getJSONObject(pos).toString();
					Log.v("min", JSONString);
					intent.putExtra("roomInfo", JSONString);

				} catch (JSONException e) {
					Log.v("min", "JSON Error");
				}
			}
			startActivity(intent);
		}
	};

	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent;
			switch (v.getId()) {
			case R.id.btn_side:
				intent = new Intent(getApplicationContext(), Side.class);
				startActivity(intent);
				break;
			default:
				break;

			}

		}
	};

	private void showActionBar() {
		LayoutInflater inflator = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflator.inflate(R.layout.actionbar, null);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setCustomView(v);
	}

	private class ViewHolder {
		public ImageView mIcon;
		public TextView mText;
	}

	private class ListViewAdapter extends BaseAdapter {
		private Context mContext = null;
		private ArrayList<ListData> mListData = new ArrayList<ListData>();

		public ListViewAdapter(Context mContext) {
			super();
			this.mContext = mContext;
		}

		@Override
		public int getCount() {
			return mListData.size();
		}

		@Override
		public Object getItem(int position) {
			return mListData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();

				LayoutInflater inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.list_item, null);

				holder.mText = (TextView) convertView.findViewById(R.id.mText);
				holder.mIcon = (ImageView) convertView.findViewById(R.id.mIcon);

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			ListData mData = mListData.get(position);
			/*
			 * if(mData.mIcon!=null) { holder.mIcon.setVisibility(View.VISIBLE);
			 * holder.mIcon.setImageDrawable(mData.mIcon); }else {
			 * holder.mIcon.setVisibility(View.GONE); }
			 */

			holder.mText.setText(mData.mText);
			holder.mIcon.setImageResource(mData.mIcon);
			return convertView;
		}

		public void addItem(String mText,int bd) {
			ListData addInfo = null;
			addInfo = new ListData();
			addInfo.mText = mText;
			addInfo.mIcon = bd;

			mListData.add(addInfo);
		}

		public void remove(int position) {
			mListData.remove(position);
			dataChange();
		}

		public void dataChange() {
			mAdapter.notifyDataSetChanged();
		}

	}
}
