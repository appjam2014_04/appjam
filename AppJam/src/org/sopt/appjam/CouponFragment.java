package org.sopt.appjam;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
public class CouponFragment extends Fragment {
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_coupon, container,
                false);
        Button button_coverPicture = (Button) root.findViewById(R.id.btn1);
        Button button_groupMessage = (Button) root.findViewById(R.id.btn3);
        Button button_out = (Button) root.findViewById(R.id.btn5);

        button_coverPicture.setOnClickListener(listener);
        button_groupMessage.setOnClickListener(listener);
        button_out.setOnClickListener(listener);

        return root;
    }


    OnClickListener listener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent intent;
            switch (v.getId()) {
                case R.id.btn1:
                    /* change cover */
                    break;
                case R.id.btn3:
                    /* sand group message */
                    break;
                case R.id.btn5:
                    /* out from group */
                    break;
                default:
                    break;

            }

        }
    };
   }