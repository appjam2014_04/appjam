package org.sopt.appjam;

import java.util.ArrayList;

import org.sopt.appjam.R;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class AttendanceFragment extends Fragment {
	
	private ListView listView = null;
	private ListViewAdapter aAdapter = null;
	   
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
   }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
            View view = inflater.inflate(R.layout.fragment_attendance, container, false);
            
            Button button_all = (Button) view.findViewById(R.id.button_at1);
            Button button_fix = (Button) view.findViewById(R.id.button_at2);
           
            button_all.setOnClickListener(listener);
            button_fix.setOnClickListener(listener);
            

            Log.v("min",""+getView());
            listView = (ListView) view.findViewById(R.id.attendance_list);
            aAdapter = new ListViewAdapter(getActivity());
            listView.setAdapter(aAdapter);
            
            aAdapter.addItem("����");
            aAdapter.addItem("�ϱ�");
            aAdapter.addItem("�÷�");
  
            return view;
    }
    
    
    
    OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent;
			switch (v.getId()) {
			case R.id.button_at1:
				intent = new Intent();
				startActivity(intent);
				break;
			case R.id.button_at2:
				intent = new Intent();
				startActivity(intent);
				break;
			default:
				break;

			}
		}
	};
	

    private class ViewHolder {
       public TextView aName;
       public TextView aAttendance;
       public ImageView aImage;
   }
   
   private class ListViewAdapter extends BaseAdapter {
      private Context aContext = null;
      private ArrayList<ListData> aListData = new ArrayList<ListData>();

      public ListViewAdapter(Context aContext) {
         super();
         this.aContext = aContext;
      }

      @Override
      public int getCount() {
         return aListData.size();
      }

      @Override
      public Object getItem(int position) {
         return aListData.get(position);
      }

      @Override
      public long getItemId(int position) {
         return position;
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
         ViewHolder holder;
         if (convertView == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) aContext
                  .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.attendance_item, null);

            holder.aImage = (ImageView) convertView.findViewById(R.id.at_image);
            holder.aName = (TextView) convertView.findViewById(R.id.at_name);
            holder.aAttendance = (TextView) convertView.findViewById(R.id.at_at);
            

            convertView.setTag(holder);
         } 
         else {
            holder = (ViewHolder) convertView.getTag();
         }

         ListData aData = aListData.get(position);
         /*
          * if(mData.mIcon!=null) { holder.mIcon.setVisibility(View.VISIBLE);
          * holder.mIcon.setImageDrawable(mData.mIcon); }else {
          * holder.mIcon.setVisibility(View.GONE); }
          */

         holder.aName.setText(aData.mText);
         //imageAdapter = new ImageAdapter(getActivity());
         //holder.mGrid.setAdapter(imageAdapter);
         return convertView;
      }

      public void addItem(String mText) {
         ListData addInfo = null;
         addInfo = new ListData();
         addInfo.mText = mText;

         aListData.add(addInfo);
      }

      public void remove(int position) {
         aListData.remove(position);
         dataChange();
      }

      public void dataChange() {
         aAdapter.notifyDataSetChanged();
      }
      
   }
}
