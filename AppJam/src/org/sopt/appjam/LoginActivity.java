package org.sopt.appjam;

import java.util.ArrayList;

import org.json.JSONArray;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		Button btn_login = (Button) findViewById(R.id.btn_login);
		TextView btn_join = (TextView) findViewById(R.id.btn_join);
		Button btn_facebook = (Button) findViewById(R.id.btn_facebook);
		btn_login.setOnClickListener(listener);
		btn_join.setOnClickListener(listener);
		btn_facebook.setOnClickListener(listener);

	}

	OnClickListener listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent intent;
			switch (v.getId()) {
			case R.id.btn_login:
				EditText id = (EditText) findViewById(R.id.edit_id);
				EditText password = (EditText) findViewById(R.id.edit_pw);
				String emailInput = id.getText().toString();
				String passInput = password.getText().toString();
				// intent = new Intent(LoginActivity.this, MainActivity.class);
				// startActivity(intent);
				try {
					String JSONString = null;
					String URL = "http://54.178.153.85:3000/users/login"; // server
																			// URL
					ArrayList<String> postInfo = new ArrayList<String>();
					postInfo.add(URL);
					postInfo.add(emailInput);
					postInfo.add(passInput);
					Post post = new Post();
					JSONString = post.execute(postInfo).get();

					JSONArray ja = new JSONArray(JSONString);
					Toast.makeText(getApplicationContext(), JSONString, Toast.LENGTH_SHORT).show();
					intent = new Intent(getApplicationContext(),
							MainActivity.class);
					intent.putExtra("roomList",JSONString);
					startActivity(intent);
					finish();

				} catch (Exception e) {
					Toast.makeText(getApplicationContext(), "Login Failed!",
							Toast.LENGTH_SHORT).show();
					Log.v("min", "fail");
				}

				break;
			case R.id.btn_join:
				intent = new Intent(LoginActivity.this, RoomEx.class);
				startActivity(intent);
				break;
			case R.id.btn_facebook:
				intent = new Intent(getApplicationContext(), JoinActivity.class);
				startActivity(intent);
				break;
			default:
				break;

			}

		}
	};
}
