package org.sopt.appjam;


import java.util.ArrayList;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class PhoneBookFragment extends Fragment {
   

   private ListView listView = null;
   private ListViewAdapter mAdapter = null;
   @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                    Bundle savedInstanceState)
    {
       View root = inflater.inflate(R.layout.fragment_phonebook, container, false);
        //View view = inflater.inflate(R.layout.fragment_gallery, container, false);
       Log.v("min",""+getView());
      listView = (ListView) root.findViewById(R.id.phone_list);
      mAdapter = new ListViewAdapter(getActivity());
      listView.setAdapter(mAdapter);

      mAdapter.addItem("����");
      mAdapter.addItem("�ϱ�");
      mAdapter.addItem("�÷�");
            return root;
    }
    private class ViewHolder {
       public TextView mName;
       public TextView mPhone;
       public ImageView mRank;
   }
   
   private class ListViewAdapter extends BaseAdapter {
      private Context mContext = null;
      private ArrayList<ListData> mListData = new ArrayList<ListData>();

      public ListViewAdapter(Context mContext) {
         super();
         this.mContext = mContext;
      }

      @Override
      public int getCount() {
         return mListData.size();
      }

      @Override
      public Object getItem(int position) {
         return mListData.get(position);
      }

      @Override
      public long getItemId(int position) {
         return position;
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
         ViewHolder holder;
         if (convertView == null) {
            holder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) mContext
                  .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.phone_item, null);

            holder.mRank = (ImageView) convertView.findViewById(R.id.mRank_phone);
            holder.mName = (TextView) convertView.findViewById(R.id.mName_phone);
            holder.mPhone = (TextView) convertView.findViewById(R.id.mPhone_phone);
            

            convertView.setTag(holder);
         } else {
            holder = (ViewHolder) convertView.getTag();
         }

         ListData mData = mListData.get(position);
         /*
          * if(mData.mIcon!=null) { holder.mIcon.setVisibility(View.VISIBLE);
          * holder.mIcon.setImageDrawable(mData.mIcon); }else {
          * holder.mIcon.setVisibility(View.GONE); }
          */

         holder.mName.setText(mData.mText);
         //imageAdapter = new ImageAdapter(getActivity());
         //holder.mGrid.setAdapter(imageAdapter);
         return convertView;
      }

      public void addItem(String mText) {
         ListData addInfo = null;
         addInfo = new ListData();
         addInfo.mText = mText;

         mListData.add(addInfo);
      }

      public void remove(int position) {
         mListData.remove(position);
         dataChange();
      }

      public void dataChange() {
         mAdapter.notifyDataSetChanged();
      }
      
         
   }
   
   
}