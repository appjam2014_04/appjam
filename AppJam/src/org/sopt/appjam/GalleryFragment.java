package org.sopt.appjam;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class GalleryFragment extends Fragment {

	private ListView listView = null;
	private ListViewAdapter mAdapter = null;
	private ImageAdapter imageAdapter = null;
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                    Bundle savedInstanceState)
    {
    	View root = inflater.inflate(R.layout.fragment_gallery, container, false);
        //View view = inflater.inflate(R.layout.fragment_gallery, container, false);
//		listView = (ListView) root.findViewById(R.id.gallery_list);
//		mAdapter = new ListViewAdapter(getActivity());
//		listView.setAdapter(mAdapter);
//
//		mAdapter.addItem("����");
//		mAdapter.addItem("�ϱ�");
//		mAdapter.addItem("�÷�");
            return root;
    }
    private class ViewHolder {
		// public ImageView icon;
    	public ViewHolder() {
			// TODO Auto-generated constructor stub
    		images = new ArrayList<ImageView>();
		}
		public TextView mDate;
    	public ArrayList<ImageView> images;
    	public GridView mGrid;
	}
	
	private class ListViewAdapter extends BaseAdapter {
		private Context mContext = null;
		private ArrayList<ListData> mListData = new ArrayList<ListData>();

		public ListViewAdapter(Context mContext) {
			super();
			this.mContext = mContext;
		}

		@Override
		public int getCount() {
			return mListData.size();
		}

		@Override
		public Object getItem(int position) {
			return mListData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				holder = new ViewHolder();

				LayoutInflater inflater = (LayoutInflater) mContext
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(R.layout.fragment_gallery_list, null);

				holder.mDate = (TextView) convertView.findViewById(R.id.mDate);
				holder.mGrid = (GridView) convertView.findViewById(R.id.picture_grid);
				

				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			ListData mData = mListData.get(position);
			/*
			 * if(mData.mIcon!=null) { holder.mIcon.setVisibility(View.VISIBLE);
			 * holder.mIcon.setImageDrawable(mData.mIcon); }else {
			 * holder.mIcon.setVisibility(View.GONE); }
			 */

			holder.mDate.setText(mData.mText);
			imageAdapter = new ImageAdapter(getActivity());
			holder.mGrid.setAdapter(imageAdapter);
			return convertView;
		}

		public void addItem(String mText) {
			ListData addInfo = null;
			addInfo = new ListData();
			addInfo.mText = mText;

			mListData.add(addInfo);
		}

		public void remove(int position) {
			mListData.remove(position);
			dataChange();
		}

		public void dataChange() {
			mAdapter.notifyDataSetChanged();
		}
		
			
	}
	
	class ImageAdapter extends BaseAdapter {
		private Context mContext;
		
		int[] picture =  {
				R.drawable.ic_launcher,
				R.drawable.ic_launcher,
				R.drawable.ic_launcher
		};
		
		public ImageAdapter(Context c) {
			mContext=c;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 100;
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return picture[position%3];
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView imageView;
			
			if(convertView == null) {
				imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(80,60));
				imageView.setAdjustViewBounds(false);
				imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			}else {
				imageView = (ImageView)convertView;
			}
			
			imageView.setImageResource(picture[position%5]);
			
			return imageView;
		}
		
	}
}
